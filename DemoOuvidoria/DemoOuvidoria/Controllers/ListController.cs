﻿using DemoOuvidoria.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DemoOuvidoria.Controllers
{
    public class ListController : Controller
    {
        // GET: List
        public ActionResult Index()
        {
            //Conexão com o Banco
            DemoOuvidoriaEntities db = new DemoOuvidoriaEntities();

            //Select na tabela Mensagem setando a uma Lista do tipo Mensagem (trazendo da tabela Mensagem do banco)
            List<Mensagem> mensagemLista = db.Mensagem.ToList();

            //Instancêando um Objeto MensagemEntidade 
            MensagemEntidade mensagemCompleta = new MensagemEntidade();

            //Criando uma Lista do tipo MensagemEntidade e recebendo a Lista setada anteriormente trazida do banco
            List<MensagemEntidade> mensagemcompletaLista = mensagemLista.Select(x => new MensagemEntidade
            {
                NomeColaborador = x.NomeColaborador,
                MensagemID = x.MensagemID,
                TelefoneColaborador = x.TelefoneColaborador,
                EmailColaborador = x.EmailColaborador,
                MensagemColaborador = x.MensagemColaborador,
                TipoMsgID = x.TipoMsgID,
                TipoMsgNome = x.TipoMensagem.TipoMsgNome
            }).ToList();

            return View(mensagemcompletaLista);

        }
    }
}