﻿using DemoOuvidoria.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;

namespace DemoOuvidoria.Controllers
{
    public class HomeController : Controller
    {

        [HttpGet]
        public ActionResult Index()
        {
            //Conexão com o Banco
            DemoOuvidoriaEntities db = new DemoOuvidoriaEntities();

            //Select recebendo os tipos de mensagem do banco
            List<TipoMensagem> lista = db.TipoMensagem.ToList();

            //Colocando esse tipo de mensagem recebido numa viewbag
            ViewBag.TipoMensagemLista = new SelectList(lista, "TipoMsgID", "TipoMsgNome");

            return View();
        }

        [HttpPost]
        public ActionResult Index(MensagemEntidade mensagem)
        {
            String assunto = "";

            //Conexão com o Banco
            DemoOuvidoriaEntities db = new DemoOuvidoriaEntities();

            //Select recebendo os tipos de mensagem do banco
            List<TipoMensagem> lista = db.TipoMensagem.ToList();

            //Colocando esse tipo de mensagem recebido numa viewbag
            ViewBag.TipoMensagemLista = new SelectList(lista, "TipoMsgID", "TipoMsgNome");

            //Pegando do Objeto e já Inserindo no Objeto vinculado ao Banco
            Mensagem msg = new Mensagem();
            msg.NomeColaborador = mensagem.NomeColaborador;
            msg.TelefoneColaborador = mensagem.TelefoneColaborador;
            msg.EmailColaborador = mensagem.EmailColaborador;
            msg.MensagemColaborador = mensagem.MensagemColaborador;
            msg.TipoMsgID = mensagem.TipoMsgID;

            //Código de Envio de E-mails
            if (ModelState.IsValid)
            {
                //Configurações de conexao de email  (skymail)
                string mailUser = "no-reply@mcmmontagens.com.br";
                string mailUserPwd = "McM@2017";
                SmtpClient client = new SmtpClient("smtp.skymail.net.br");
                client.Port = 25;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.UseDefaultCredentials = false;
                NetworkCredential credentials = new NetworkCredential(mailUser, mailUserPwd);
                client.EnableSsl = true;
                client.Credentials = credentials;

                //Enviando e-mail de confirmação a pessoa que enviou a mensagem
                MailAddress to = new MailAddress(msg.EmailColaborador);
                MailMessage message = new MailMessage(mailUser, mensagem.EmailColaborador);
                message.Subject = "Seu Contato com a MCM";
                message.Body = "MCM informa:\nRecebemos sua mensagem: \n" + mensagem.MensagemColaborador + "\n\nAtenciosamente\nMCM Montagens Industriais";
                Console.WriteLine("Sending an e-mail message to {0} by using the SMTP host {1}.", to.Address, client.Host);

                try
                {
                    client.Send(message); //Enviando e-mail
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Exception caught in CreateCopyMessage(): {0}", ex.ToString());
                }

                //Enviando o e-mail a pessoa responsável, dependendo do assunto (tipo de msg) escolhida.
                //Se for Reclamação para Mozart, se não, para Rayne (botei para os meus e-mails como exemplo e teste)
                String to2 = "";
                String from2 = mailUser;
                if (mensagem.TipoMsgID == 2)
                {
                    assunto = "Reclamação Fale Conosco";
                    to2 = "glauber.araujo@mcmmontagens.com.br";
                    //MailAddress to2 = Mozart

                }
                else
                {
                    to2 = "glauberhot7@hotmail.com";
                    //MailAddress to2 = Rayne

                    switch (mensagem.TipoMsgID)
                    {
                        case 4:
                            assunto = "Dúvida Fale Conosco";
                            break;
                        case 1:
                            assunto = "Sugestão Fale Conosco";
                            break;
                        case 3:
                            assunto = "Denúncia Fale Conosco";
                            break;
                    }
                }

                //O corpo e titulo do e-mail
                MailMessage message2 = new MailMessage(from2, to2);
                message2.Subject = assunto;
                message2.Body = "Você recebeu a seguinte mensagem do Fale Conosco: \n" + mensagem.MensagemColaborador + "\n" + assunto + "\n\nAtt.";

                try
                {
                    client.Send(message2); //Enviando e-mail
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Exception caught in CreateCopyMessage(): {0}",
                                ex.ToString());
                }

            }

            //Adicionado o objeto msg a classe/tabela Mensagem do banco de dados
            db.Mensagem.Add(msg);
            db.SaveChanges();

            //Armazenando o ultimo Id da mensagem cadastrada a uma variável.
            int ultimaMsgId = msg.MensagemID;

            // Chamando a View Index que vai receber esse objeto mensagem já preenchido
            return View(mensagem);
        }

    }

}