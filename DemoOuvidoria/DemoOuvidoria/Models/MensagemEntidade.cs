﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DemoOuvidoria.Models
{
    public class MensagemEntidade
    {
        //Atributos
        public int MensagemID { get; set; }

        [StringLength(30, ErrorMessage = "Nome não pode ultrapassar 30 caracteres.")]
        public string NomeColaborador { get; set; }

        [StringLength(20, ErrorMessage = "Telefone não pode ultrapassar 20 caracteres.")]
        public string TelefoneColaborador { get; set; }

        [StringLength(40, ErrorMessage = "E-mail não pode ultrapassar 40 caracteres.")]
        [Required(ErrorMessage = "Por favor, Insira seu E-mail.")]
        public string EmailColaborador { get; set; }

        [StringLength(250, ErrorMessage = "Mensagem não pode ultrapassar 250 caracteres.")]
        [Required(ErrorMessage = "Por favor, Insira a Mensagem que deseja enviar.")]
        public string MensagemColaborador { get; set; }

        [Required(ErrorMessage = "Por favor, Escolha o tipo de Mensagem que deseja enviar.")]
        public int TipoMsgID { get; set; }

        //Atributos adicionais
        public string TipoMsgNome { get; set; }
    }
}